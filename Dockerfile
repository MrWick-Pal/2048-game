FROM node:16-alpine as node16
WORKDIR /app
COPY 2048-game/ .
RUN npm install --include=dev && npm run build

FROM nginx:1.25.3-alpine
WORKDIR /app
COPY --from=node16 /app /app
COPY 2048-game/dist/index.html /usr/share/nginx/html/index.html
COPY default.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]


